import 'package:flutter/material.dart';

class Home2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        alignment: Alignment.center,
        color: Colors.tealAccent,
        width: 300.0,
        height: 300.0,
        child: Column(children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(child:
                Text("row 1",
                style: TextStyle(
                  fontSize: 10.0,
                  decoration: TextDecoration.overline
                )
              )),
              Expanded(child: Text ("a long row of text,really long",
                      style: TextStyle(
                        fontSize: 15.0
                      ),),)
          ],
        ),

        Row(
          children: <Widget>[
            Expanded(child:
              Text("row 2",
              style: TextStyle(
                fontSize: 10.0,
                decoration: TextDecoration.overline
              )
            )),
            Expanded(
              child: Text ("a longeeeer row of text,really long",
                      style: TextStyle(
                        fontSize: 15.0
                      ),),)
          ],
        )


        ],)
         
        
        
        
      )
    );
    }
 
}