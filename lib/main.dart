import 'package:flutter/material.dart';
import './ui/home2.dart';
//import './screens/home.dart';

void main() => runApp(new DemoApp());

class DemoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: new Home2(),
      title: 'flutter demo',
    );
  }
}